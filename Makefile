.PHONY: all compile clean cleanall distclean serve

all: compile

compile:
	rebar3 compile

clean:
	rebar3 clean

cleanall:
	rebar3 clean --all

distclean:
	rm -rf _build built.flag
	$(MAKE) dialclean

serve:
	erl -pa _build/default/lib/*/ebin -s websrv serve

.PHONY: plt dial dialclean

plt:
	#dialyzer --build_plt --output_plt .dialyzer_plt -o .dialyzer_plt.out.txt --apps erts kernel stdlib -c deps/*/ebin
	dialyzer --build_plt --output_plt .dialyzer_plt -o .dialyzer_plt.out.txt --apps erts kernel stdlib

.dialyzer_plt: $(wildcard deps/ebin/*)
	$(MAKE) plt

dial: .dialyzer_plt
	#dialyzer --plt .dialyzer_plt -o dialyzer.out.txt ebin || (cat dialyzer.out.txt; exit 1)
	dialyzer --plt .dialyzer_plt -o dialyzer.out.txt deps/*/ebin ebin || (cat dialyzer.out.txt; exit 1)

dialclean:
	rm -f .dialyzer_plt .dialyzer_plt.out.txt dialyzer.out.txt
