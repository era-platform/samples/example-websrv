%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Dummy application using era-platform/webserverlib.
%%% -------------------------------------------------------------------

-module(websrv).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(application).

%% application callbacks
-export([start/2, stop/1]).

%% extra functions
-export([serve/0]).

-include("websrv.hrl").

%% ===================================================================
%% Definitions
%% ===================================================================

-define(WSLH_STATIC_DIR, webserverlib_handler_static).

%% ===================================================================
%% Callbacks
%% ===================================================================

%% -------------------------------------------------------------------
-spec start(StartType :: application:start_type(), StartArgs :: term()) ->
    {ok, pid()} | {ok, pid(), State :: term()} | {error, Reason :: term()}.
%% -------------------------------------------------------------------
start(_StartType,_StartArgs) ->
    add_deps_paths(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(_StartArgs).

%% @private
add_deps_paths() ->
    DN = fun(P) -> filename:dirname(P) end,
    AppsDir = DN(DN(DN(code:which(?MODULE)))),
    DepApps = [webserverlib, loggerlib] ++ [basiclib, cowboy, cowlib, iconverl, jsx, ranch],
    DepPaths = [filename:join([AppsDir, Dep, "ebin"]) || Dep <- DepApps],
    code:add_pathsa(DepPaths).

%% @private
setup_dependencies() ->
    Routes = [{"/[...]", ?WSLH_STATIC_DIR, [{static_opts, {priv_dir,?APP,"www",[{mimetypes,cow_mimetypes,all}]} },
                                            %{static_idx, "index.html"}, % is already by default
                                            {static404, {priv_file,?APP,"www/index.html"}}]}],
    Listener = #{protocol => http,
                 port => 8181,
                 addr => "0.0.0.0",
                 routes => Routes},
    ?BU:set_env(?WSL, listener_default, Listener).

%% @private
ensure_deps_started() ->
    WslArgs = [],
    webserverlib:start(normal, WslArgs). % THIS ONE TRIGGERS START

%% -------------------------------------------------------------------
stop(_State) -> ok.

%% ====================================================================
%% Public API
%% ====================================================================

serve() ->
    add_deps_paths(),
    application:ensure_all_started(?APP).

%% ====================================================================
%% Internal
%% ====================================================================

