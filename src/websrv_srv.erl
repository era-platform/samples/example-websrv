%%% -*- coding: utf-8 -*-
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Anton Makarov <anton@mastermak.ru>.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>
%%% @date 18.02.2021
%%% @doc Dummy application gen_server. Runs tests over webserverlib.
%%% -------------------------------------------------------------------

-module(websrv_srv).
-author('Anton Makarov <anton@mastermak.ru>').

-behaviour(gen_server).

-export([start_link/0]).
-export([start_link/1]).

-export([init/1, handle_call/3, handle_cast/2]).
-export([handle_info/2]).

-include("websrv.hrl").

%% ===================================================================
%% Definitions
%% ===================================================================

-record(state, {
          start_args :: term(),
          seq :: integer(),
          success :: boolean(),
          pid :: pid(),
          mon_ref :: reference()
         }).

-define(LastSeq, 4).

%% ===================================================================
%% Public
%% ===================================================================

start_link() ->
    start_link([start_link,$/,$0]).

start_link(StartArgs) ->
    %ok = inets:start(), % is started by .app.src
    gen_server:start_link({local,?MODULE}, ?MODULE, StartArgs, []).

%% ===================================================================
%% Callbacks
%% ===================================================================

init(StartArgs) ->
    ?OUT("~p:init(StartArgs = ~120tp)", [?MODULE, StartArgs]),
    State = #state{start_args = StartArgs},
    {ok, State}.

%% -------------------------------------------------------------------
handle_call(_Request, _From, State) ->
    ?OUT("~p:handle_call(~120tp, From=~120tp, State) - ignored", [?MODULE, _Request, _From]),
    {noreply, State}.

%% -------------------------------------------------------------------
handle_cast(_Request, State) ->
    ?OUT("~p:handle_cast(~120tp, State) - ignored", [?MODULE, _Request]),
    {noreply, State}.

%% -------------------------------------------------------------------
handle_info(_Request, State) ->
    ?OUT("~p:handle_info(~120tp, State) - ignored", [?MODULE, _Request]),
    {noreply, State}.

%% ===================================================================
%% Internal
%% ===================================================================
